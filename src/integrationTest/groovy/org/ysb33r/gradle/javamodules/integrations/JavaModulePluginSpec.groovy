/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules.integrations

import org.gradle.testkit.runner.TaskOutcome

class JavaModulePluginSpec extends IntegrationSpecification {

    void setup() {
        writeJavaModuleFile()
        writeJavaSourceFile()
    }

    void 'Can describe extra modules'() {
        setup:
        buildFile << """
        repositories {
            mavenCentral()
        }
    
        dependencies {
            implementation 'commons-cli:commons-cli:1.4'
        }
        
        jpmsExtraModules {
            module("commons-cli-1.4.jar","commons-cli","1.4") {
                exports('org.apache.commons.cli')
            }
        }
        """

        when:
        def result = getGradleRunner(IS_GROOVY_DSL, projectDir, ['assemble', '-i']).build()

        then:
        result.task(':assemble').outcome == TaskOutcome.SUCCESS
    }

    void 'Can make module from a dependency'() {
        setup:
        buildFile << """
        repositories {
            mavenCentral()
        }
    
        dependencies {
            implementation jpms.asModule('commons-cli:commons-cli:1.4','org.apache.commons.cli')
        }
        """

        when:
        def result = getGradleRunner(IS_GROOVY_DSL, projectDir, ['assemble', '-i']).build()

        then:
        result.task(':assemble').outcome == TaskOutcome.SUCCESS
    }

    void 'Can make module from a dependency and set module name'() {
        setup:
        buildFile << """
        repositories {
            mavenCentral()
        }
    
        dependencies {
            implementation jpms.asModule('commons-cli:commons-cli:1.4','org.apache.commons.cli') {
                moduleName = 'commons.cli'
            }
        }
        """

        when:
        def result = getGradleRunner(IS_GROOVY_DSL, projectDir, ['assemble', '-i']).build()

        then:
        result.task(':assemble').outcome == TaskOutcome.SUCCESS
    }

    void writeJavaModuleFile() {
        javaModuleInfo << """
        module ${packageName} {
            requires org.apache.commons.cli;
            exports ${packageName};
        }
        """
    }

    void writeJavaSourceFile() {
        new File(packageDir,'Foo.java') << """
        package ${packageName};
        import org.apache.commons.cli.HelpFormatter;
        class Foo {
            public static HelpFormatter HELP = new HelpFormatter();
        }
        """
    }
}