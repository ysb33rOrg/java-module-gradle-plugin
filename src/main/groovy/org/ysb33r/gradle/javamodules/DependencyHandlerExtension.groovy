/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.ysb33r.gradle.javamodules.internal.DependencyAsModuleDetails
import org.ysb33r.gradle.javamodules.internal.ModuleJarUtils

import static groovy.lang.Closure.DELEGATE_ONLY

@CompileStatic
class DependencyHandlerExtension {

    DependencyHandlerExtension(
        ExtraModuleInfoPluginExtension extraModules,
        DependencyHandler dependencies
    ) {
        this.extraModules = extraModules
        this.dependencies = dependencies
    }

    Dependency asModule(CharSequence gav, CharSequence exported) {
        asModule(gav, exported) { }
    }

    Dependency asModule(CharSequence gav, CharSequence exported, Action<CoercedModule> configurator) {
        def dam = new DependencyAsModuleDetails(
            dependency: dependencies.create(gav),
        )
        dam.exports(exported)
        configurator.execute(dam)
        ModuleJarUtils.moduleFromGAV(extraModules, dam)
        dam.dependency
    }

    Dependency asModule(CharSequence gav, CharSequence exported, @DelegatesTo(CoercedModule) Closure configurator) {
        Closure cfg = (Closure) configurator.clone()
        def dam = new DependencyAsModuleDetails(
            dependency: dependencies.create(gav),
        )
        dam.exports(exported)
        cfg.delegate = dam
        cfg.resolveStrategy = DELEGATE_ONLY
        cfg.call()
        ModuleJarUtils.moduleFromGAV(extraModules, dam)
        dam.dependency
    }

    private final ExtraModuleInfoPluginExtension extraModules
    private final DependencyHandler dependencies
}
