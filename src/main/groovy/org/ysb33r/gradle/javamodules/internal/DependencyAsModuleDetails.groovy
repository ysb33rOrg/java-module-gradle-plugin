/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules.internal

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.Dependency
import org.ysb33r.gradle.javamodules.CoercedModule
import org.ysb33r.grolifant.api.v4.StringUtils

@CompileStatic
class DependencyAsModuleDetails implements CoercedModule {
    String moduleName
    Dependency dependency
    final List<String> exports = []

    @Override
    void exports(CharSequence... exportedPackages) {
        this.exports.addAll(StringUtils.stringize(exportedPackages as List))
    }
}
