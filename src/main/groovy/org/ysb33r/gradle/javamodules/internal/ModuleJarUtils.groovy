/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules.internal

import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType
import groovy.util.logging.Slf4j
import org.gradle.api.NamedDomainObjectSet
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.api.provider.Provider
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ModuleVisitor
import org.objectweb.asm.Opcodes
import org.ysb33r.gradle.javamodules.ExtraModuleInfoPluginExtension
import org.ysb33r.gradle.javamodules.ModuleInfo
import org.ysb33r.gradle.javamodules.ModuleTransformException

import java.util.jar.Attributes
import java.util.jar.JarEntry
import java.util.jar.JarInputStream
import java.util.jar.JarOutputStream
import java.util.jar.Manifest
import java.util.regex.Pattern
import java.util.zip.ZipEntry

import static org.gradle.api.plugins.JavaPlugin.ANNOTATION_PROCESSOR_CONFIGURATION_NAME
import static org.gradle.api.plugins.JavaPlugin.COMPILE_CLASSPATH_CONFIGURATION_NAME
import static org.gradle.api.plugins.JavaPlugin.RUNTIME_CLASSPATH_CONFIGURATION_NAME
import static org.objectweb.asm.Opcodes.ACC_MODULE
import static org.objectweb.asm.Opcodes.ACC_OPEN
import static org.objectweb.asm.Opcodes.V9

/** Utilities dealing with creating module JARs.
 *
 */
@CompileStatic
@Slf4j
class ModuleJarUtils {
    public static final Pattern MODULE_INFO_CLASS =
        Pattern.compile('META-INF/versions/\\d+/module-info.class')

    /** Return a list of configurations that should be inspected for modules.
     *
     * @param project
     * @return
     */
    static NamedDomainObjectSet<Configuration> moduleConfigurations(Project project) {
        project.configurations.matching { Configuration it ->
            if (!it.canBeResolved) {
                return false
            }
            it.name.endsWith(COMPILE_CLASSPATH_CONFIGURATION_NAME.substring(1)) ||
                it.name.endsWith(RUNTIME_CLASSPATH_CONFIGURATION_NAME.substring(1)) ||
                it.name.endsWith(ANNOTATION_PROCESSOR_CONFIGURATION_NAME.substring(1))
        }
    }

    /**
     * A provider of all resolved dependencies for configurations that are of importance for JPMS.
     *
     * @param project
     * @return
     */
    static Provider<Set<ResolvedDependency>> resolvedDependencyProvider(Project project) {
        project.provider { ->
            moduleConfigurations(project)*.resolvedConfiguration*.firstLevelModuleDependencies
                .flatten().toSet() as Set<ResolvedDependency>
        }
    }

    /**
     * Works out module deatils from Movan GAV.
     *
     * @param emi Extension where to write module into to
     * @param moduleDetails Details for creating module info from information that user provided when declaring a
     *   dependency.
     */
    static void moduleFromGAV(
        ExtraModuleInfoPluginExtension emi,
        DependencyAsModuleDetails moduleDetails
    ) {
        Dependency dep = moduleDetails.dependency
        String jarName = "${dep.name}-${dep.version}.jar"
        String moduleName = moduleDetails.moduleName ?: dep.name.replace('-', '.')

        emi.module(jarName, moduleName, dep.version) { ModuleInfo module ->
            moduleDetails.exports.each { exportedPackage ->
                module.exports(exportedPackage)
            }
            log.info "Registering module ${moduleName} (${dep.version}) from ${jarName} with exports" +
                " ${moduleDetails.exports}"
        }
    }

    /**
     * Checks whether JAR has an automatic module entry.
     *
     * @param jar JAR to process
     * @return {@code true} if {@code Automatic-Module-Name} exists.
     */
    static boolean isAutoModule(File jar) {
        doWithJar(jar) { JarInputStream inputStream ->
            inputStream.manifest.mainAttributes.getValue(AUTO_MODULE) != null
        }
    }

    /**
     * Checks for multi-release JARs
     *
     * @param jarStream JAR Stream to scan
     * @return {@code true} if {@code Multi-Release} exists
     */
    static boolean containsMultiReleaseJarEntry(JarInputStream jarStream) {
        Manifest manifest = jarStream.manifest
        manifest != null && Boolean.parseBoolean(manifest.mainAttributes.getValue('Multi-Release'))
    }

    /** Checks whether the module is a JAR.
     *
     * @param jar JAR to process.
     * @return {@code true} if it is a module jar.
     */
    static boolean isModule(File jar) {
        doWithJar(jar) { JarInputStream inputStream ->
            boolean isMultiReleaseJar = containsMultiReleaseJarEntry(inputStream)
            ZipEntry next = inputStream.nextEntry
            while (next != null) {
                if (next.name == MODULE_CLASS ||
                    isMultiReleaseJar && MODULE_INFO_CLASS.matcher(next.name).matches()) {
                    return true
                }
                next = inputStream.nextEntry
            }
            false
        }
    }

    /** Reads a JAR and performs action on entries in the JAR.
     *
     * @param originalJar JAR to read
     * @param action Action to perform. Takes a {@Link JarInputStream} as parameter.
     * @return Returns the result of the action.
     */
    static <R> R doWithJar(
        File originalJar,
        @ClosureParams(value = SimpleType, options = 'java.util.jar.JarInputStream') Closure<R> action
    ) {
        try {
            new FileInputStream(originalJar).withCloseable { fstream ->
                new JarInputStream(fstream).withCloseable { inputStream ->
                    action(inputStream)
                }
            }
        } catch (IOException e) {
            throw new ModuleTransformException(e)
        }
    }

    static void addAutomaticModuleName(File originalJar, File moduleJar, String moduleName) {
        doWithJar(originalJar) { JarInputStream inputStream ->
            Manifest manifest = inputStream.manifest
            manifest.mainAttributes.put(new Attributes.Name(AUTO_MODULE), moduleName)
            doWithModuleJar(inputStream, moduleJar) { JarOutputStream outputStream ->
                copyEntries(inputStream, outputStream)
            }
        }
    }

    static void addModuleDescriptor(File originalJar, File moduleJar, ModuleInfo moduleInfo) {
        doWithJar(originalJar) { JarInputStream inputStream ->
            doWithModuleJar(inputStream, moduleJar) { JarOutputStream outputStream ->
                copyEntries(inputStream, outputStream)
                outputStream.putNextEntry(new JarEntry(MODULE_CLASS))
                outputStream.write(addModuleInfo(moduleInfo))
                outputStream.closeEntry()
            }
        }
    }

    private static void doWithModuleJar(
        JarInputStream inputStream,
        File moduleJar,
        @ClosureParams(value = SimpleType, options = 'java.util.jar.JarOutputStream') Closure action
    ) {
        moduleJar.withOutputStream { fstream ->
            new JarOutputStream(fstream, inputStream.manifest).withCloseable { JarOutputStream outputStream ->
                action(outputStream)
            }
        }
    }

    private static void copyEntries(JarInputStream inputStream, JarOutputStream outputStream) throws IOException {
        JarEntry jarEntry = inputStream.nextJarEntry
        while (jarEntry != null) {
            outputStream.putNextEntry(jarEntry)
            outputStream.write(inputStream.readAllBytes())
            outputStream.closeEntry()
            jarEntry = inputStream.nextJarEntry
        }
    }

    @SuppressWarnings('DuplicateStringLiteral')
    private static byte[] addModuleInfo(ModuleInfo moduleInfo) {
        ClassWriter classWriter = new ClassWriter(0)
        classWriter.visit(V9, ACC_MODULE, 'module-info', null, null, null)
        ModuleVisitor moduleVisitor = classWriter.visitModule(
            moduleInfo.moduleName,
            ACC_OPEN,
            moduleInfo.moduleVersion
        )
        for (String packageName : (moduleInfo.exports)) {
            moduleVisitor.visitExport(packageName.replace('.', '/'), 0)
        }
        moduleVisitor.visitRequire('java.base', 0, null)
        for (String requireName : (moduleInfo.requires)) {
            moduleVisitor.visitRequire(requireName, 0, null)
        }
        for (String requireName : (moduleInfo.requiresTransitive)) {
            moduleVisitor.visitRequire(requireName, Opcodes.ACC_TRANSITIVE, null)
        }
        moduleVisitor.visitEnd()
        classWriter.visitEnd()
        classWriter.toByteArray()
    }

    private static final String MODULE_CLASS = 'module-info.class'
    private static final String AUTO_MODULE = 'Automatic-Module-Name'
}
