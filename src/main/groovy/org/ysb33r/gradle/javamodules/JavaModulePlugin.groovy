/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.transform.TransformSpec
import org.gradle.api.attributes.Attribute
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension

import static org.ysb33r.gradle.javamodules.internal.ModuleJarUtils.moduleConfigurations

/**
 * Entry point of our plugin that should be applied in the root project.
 */
@CompileStatic
class JavaModulePlugin implements Plugin<Project> {

    public static final String EXTENSION_NAME = 'jpmsExtraModules'
    public static final String DEP_EXTENSION_NAME = 'jpms'

    @Override
    void apply(Project project) {
        ExtraModuleInfoPluginExtension extension = project.extensions.create(
            EXTENSION_NAME,
            ExtraModuleInfoPluginExtension
        )

        extendDependencyHandler(project, extension)

        project.plugins.withType(JavaPlugin).configureEach {
            project.extensions.getByType(JavaPluginExtension).modularity.inferModulePath.set(true)
            configureTransform(project, extension)
        }
    }

    void configureTransform(Project project, ExtraModuleInfoPluginExtension extension) {
        Attribute<String> artifactType = Attribute.of('artifactType', String)
        Attribute<Boolean> javaModule = Attribute.of('javaModule', Boolean)
        DependencyHandler deps = project.dependencies

        moduleConfigurations(project).all { Configuration it ->
            it.attributes.attribute(javaModule, true)
        }

        deps.artifactTypes.getByName(JAR_TYPE).attributes.attribute(javaModule, false)
        deps.registerTransform(ExtraModuleInfoTransform) { TransformSpec it ->
            it.with {
                parameters { ModuleTransformParameter mtp ->
                    mtp.moduleInfo = extension.moduleInfo
                    mtp.automaticModules = extension.automaticModules
//                    mtp.processTransitives.set(true)
                }
                from.attribute(artifactType, JAR_TYPE).attribute(javaModule, false)
                to.attribute(artifactType, JAR_TYPE).attribute(javaModule, true)
            }
        }
    }

    private void extendDependencyHandler(Project project, ExtraModuleInfoPluginExtension ext) {
        ((ExtensionAware) project.dependencies).extensions.create(
            DEP_EXTENSION_NAME,
            DependencyHandlerExtension,
            ext,
            project.dependencies
        )
    }

    private static final String JAR_TYPE = 'jar'
}
