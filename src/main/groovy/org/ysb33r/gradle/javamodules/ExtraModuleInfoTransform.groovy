/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.artifacts.transform.CacheableTransform
import org.gradle.api.artifacts.transform.InputArtifact
import org.gradle.api.artifacts.transform.TransformAction
import org.gradle.api.artifacts.transform.TransformOutputs
import org.gradle.api.file.FileSystemLocation
import org.gradle.api.provider.Provider

import static org.ysb33r.gradle.javamodules.internal.ModuleJarUtils.addAutomaticModuleName
import static org.ysb33r.gradle.javamodules.internal.ModuleJarUtils.addModuleDescriptor
import static org.ysb33r.gradle.javamodules.internal.ModuleJarUtils.isAutoModule
import static org.ysb33r.gradle.javamodules.internal.ModuleJarUtils.isModule

/**
 * An artifact transform that applies additional information to Jars without module information.
 * The transformation fails the build if a Jar does not contain information and no extra information
 * was defined for it. This way we make sure that all Jars are turned into modules.
 */
@CompileStatic
//@CacheableTransform
@Slf4j
abstract class ExtraModuleInfoTransform implements TransformAction<ModuleTransformParameter> {

    @Override
    void transform(TransformOutputs outputs) {
        Map<String, ModuleInfo> moduleInfo = parameters.moduleInfo
        Map<String, String> automaticModules = parameters.automaticModules

        File originalJar = inputArtifact.get().asFile
        String originalJarName = originalJar.name

        if (isModule(originalJar)) {
            outputs.file(originalJar)
            log.debug("transform: ${originalJarName} is already a module")
        } else if (moduleInfo.containsKey(originalJarName)) {
            File newJar = getModuleJar(outputs, originalJar)
            ModuleInfo info = moduleInfo.get(originalJarName)
            addModuleDescriptor(originalJar, newJar , info)
            log.debug("transform: added module descriptor: ${originalJarName} => ${newJar.name}. ${info}")
        } else if (isAutoModule(originalJar)) {
            outputs.file(originalJar)
            log.debug("transform: ${originalJarName} is an auto-module")
        } else if (automaticModules.containsKey(originalJarName)) {
            File newJar = getModuleJar(outputs, originalJar)
            addAutomaticModuleName(
                originalJar,
                newJar,
                automaticModules.get(originalJarName)
            )
            log.debug("transform: added automatic module name:  ${originalJarName} => ${newJar.name}")
        } else {
            throw new ModuleTransformException("Not a module and no mapping defined: ${originalJarName}")
        }
    }

    @SuppressWarnings('UnnecessarySubstring')
    File getModuleJar(TransformOutputs outputs, File originalJar) {
        outputs.file("${originalJar.name.substring(0, originalJar.name.lastIndexOf('.'))}-module.jar")
    }

    @InputArtifact
    protected abstract Provider<FileSystemLocation> getInputArtifact()
}
