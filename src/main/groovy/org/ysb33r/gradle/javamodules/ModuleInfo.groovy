/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.javamodules

import groovy.transform.CompileStatic

/**
 * Data class to hold the information that should be added as module-info.class to an existing Jar file.
 */
@CompileStatic
class ModuleInfo implements Serializable {

    ModuleInfo(String moduleName, String moduleVersion) {
        this.moduleName = moduleName
        this.moduleVersion = moduleVersion
    }

    boolean transitive = false

    void exports(String exports) {
        this.exports.add(exports)
    }

    void requires(String requires) {
        this.requires.add(requires)
    }

    void requiresTransitive(String requiresTransitive) {
        this.requiresTransitive.add(requiresTransitive)
    }

    String getModuleName() {
        this.moduleName
    }

    String getModuleVersion() {
        this.moduleVersion
    }

    List<String> getExports() {
        this.exports
    }

    List<String> getRequires() {
        this.requires
    }

    List<String> getRequiresTransitive() {
        this.requiresTransitive
    }

    @Override
    String toString() {
        "ModuleInfo: name=${moduleName}, version=${moduleVersion}, exports=${exports}, requires=${requires}, " +
            "requiresTransitive=${requiresTransitive}"
    }

    private final String moduleName
    private final String moduleVersion
    private final List<String> exports = []
    private final List<String> requires = []
    private final List<String> requiresTransitive = []

}
